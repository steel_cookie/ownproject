package com.kostiantyn.honcharenko;

import com.kostiantyn.honcharenko.dao.ProductDao;
import com.kostiantyn.honcharenko.dao.UserDao;
import com.kostiantyn.honcharenko.entity.Product;
import com.kostiantyn.honcharenko.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringProjectApplication {

	@Autowired
	private UserDao userDao;

    @Autowired
    private ProductDao productDao;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public static void main(String[] args) {
		SpringApplication.run(SpringProjectApplication.class, args);
	}

    @PostConstruct
    public void init() {


       /* userDao.save(new User("Kostiantyn", "Honcharenko", "admin", passwordEncoder.encode("admin"), true));
        userDao.save(new User("qwe", "qwe", "qwe", passwordEncoder.encode("qwe"), false));

        productDao.save(new Product("Jeans", 13, 26));
        productDao.save(new Product("Pen", 7, 2.5));
        productDao.save(new Product("Table", 4, 99.5)); */
    }


}
