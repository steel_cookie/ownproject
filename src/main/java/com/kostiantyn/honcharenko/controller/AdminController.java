package com.kostiantyn.honcharenko.controller;

import com.kostiantyn.honcharenko.dao.ProductDao;
import com.kostiantyn.honcharenko.dao.UserDao;
import com.kostiantyn.honcharenko.entity.Product;
import com.kostiantyn.honcharenko.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

@Controller
public class AdminController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

    //------------------> management of users

    @RequestMapping(value = "doAdmin/{userId}" , method = RequestMethod.GET)
    public String doAdmin(@PathVariable(value="userId") String id, Principal principal) {
        User user = userDao.findByUserid(Integer.parseInt(id));
        if (userDao.findByLogin(principal.getName()).isAdmin()) {
            if (!user.isAdmin()) {
                user.setAdmin(true);
            } else {
                user.setAdmin(false);
            }
        }
        userDao.save(user);
        return "redirect:/users";
    }

    @RequestMapping(value = "deleteUser/{userId}" , method = RequestMethod.GET)
    public String deleteUser(@PathVariable(value="userId") String id, Principal principal) {
        if (userDao.findByLogin(principal.getName()).isAdmin()) {
            userDao.deleteById(Integer.parseInt(id));
        }
        return "redirect:/users";
    }

    //------------------> management of products

    @RequestMapping (value = "/addProduct" , method =RequestMethod.GET)
    public String addProduct(Model m) {
        m.addAttribute("product", new Product());
        return "addProduct";
    }

    @RequestMapping(value = "/addProduct" , method = RequestMethod.POST)
    public String addProductPOST(@ModelAttribute(value = "user") Product product) {
        productDao.save(product);
        return "redirect:/products";
    }

    @RequestMapping (value = "/editProduct/{product.productid}" , method =RequestMethod.GET)
    public String editProduct(@PathVariable(value="product.productid") String id, Model m) {
        Product product = productDao.findByProductid(Integer.parseInt(id));
        m.addAttribute("product", product);
        return "editProduct";
    }

    @RequestMapping (value = "/editProduct" , method =RequestMethod.POST)
    public String editProductPOST(@ModelAttribute(value="product") Product product) {
        productDao.save(product);
        return "redirect:/products";
    }

    @RequestMapping(value = "deleteProduct/{product.productid}" , method = RequestMethod.GET)
    public String deleteProduct(@PathVariable(value="product.productid") String id, Principal principal) {
        if (userDao.findByLogin(principal.getName()).isAdmin()) {
            productDao.deleteById(Integer.parseInt(id));
        }
        return "redirect:/products";
    }

}
