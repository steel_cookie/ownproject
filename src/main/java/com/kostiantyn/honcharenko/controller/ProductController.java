package com.kostiantyn.honcharenko.controller;

import com.kostiantyn.honcharenko.dao.ProductDao;
import com.kostiantyn.honcharenko.dao.UserDao;
import com.kostiantyn.honcharenko.entity.Product;
import com.kostiantyn.honcharenko.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
public class ProductController {

    @Autowired
    private ProductDao productDao;

    @Autowired
    private UserDao userDao;

   /* @RequestMapping (value = "/layout", method = RequestMethod.GET)
    public String layoutPage(Model m, Principal principal) {
        if (dao.findByLogin(principal.getName())==null) {
            m.addAttribute("access", 0);
        }
        return "login";
    }*/

    @RequestMapping(value = "/products", method = RequestMethod.GET)
    public String allProductsPage(Model m, Principal principal) {
        m.addAttribute("isAdmin", userDao.findByLogin(principal.getName()).isAdmin());
        m.addAttribute("productlist", productDao.findAll());
        return "products";
    }

        @RequestMapping (value = "/buyProduct/{product.productid}" , method =RequestMethod.GET)
    public String buyProduct(@PathVariable(value="product.productid") String id, Principal principal) {
        int buyingProductId = Integer.parseInt(id);

        Product product = productDao.findByProductid(buyingProductId);
        int newQuantity = (product.getQuantity()-1 >= 0) ? product.getQuantity()-1 : 0;
        product.setQuantity(newQuantity);

        User user = userDao.findByLogin(principal.getName());
        user.buyProduct(buyingProductId);

        productDao.save(product);
        userDao.save(user);
        return "redirect:/products";
    }

}
