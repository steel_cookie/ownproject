package com.kostiantyn.honcharenko.controller;

import com.kostiantyn.honcharenko.dao.ProductDao;
import com.kostiantyn.honcharenko.dao.UserDao;
import com.kostiantyn.honcharenko.entity.Product;
import com.kostiantyn.honcharenko.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ProductDao productDao;

   /* @RequestMapping (value = "/layout", method = RequestMethod.GET)
    public String layoutPage(Model m, Principal principal) {
        if (dao.findByLogin(principal.getName())==null) {
            m.addAttribute("access", 0);
        }
        return "login";
    }*/

    @RequestMapping (value = "/login", method = RequestMethod.GET)
    public String loginPage() {
            return "login";
    }

    @RequestMapping (value = "/register" , method =RequestMethod.GET)
    public String registerPage(Model m) {
            m.addAttribute("user", new User());
            return "register";
    }
    @RequestMapping(value = "/register" , method = RequestMethod.POST)
    public String registerPagePOST(@ModelAttribute(value = "user") User user) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        userDao.save(user);
            return "redirect:/login";
    }
    @RequestMapping(value = "/profile" , method = RequestMethod.GET)
    public String profilePage(Model m, Principal principal) {
            User user = userDao.findByLogin(principal.getName());


            HashMap <Integer, Integer> boughtP = user.getBoughtProducts();
            List<Product> productList = productDao.findAll();
            List<Product> boughtProducts = new ArrayList<>();

            for (Product product : productList) {
                if (boughtP.containsKey(product.getProductid())) {
                    Product tmpProduct = product;
                    tmpProduct.setQuantity(boughtP.get(product.getProductid()));
                    tmpProduct.setPrice(tmpProduct.getPrice() * tmpProduct.getQuantity());
                    boughtProducts.add(tmpProduct);
                }
            }

            m.addAttribute("boughtProducts", boughtProducts);
            m.addAttribute("user", user);
            return "profile";
    }
    @RequestMapping(value = "/users" , method = RequestMethod.GET)
    public String profileUsersPage(Model m, Principal principal) {
        if (userDao.findByLogin(principal.getName())!=null) {
            m.addAttribute("isAdmin", userDao.findByLogin(principal.getName()).isAdmin());
            m.addAttribute("userlist", userDao.findAll());
            return "users";
        } else {
            return "redirect:/login";
        }
    }
}
