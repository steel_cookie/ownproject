package com.kostiantyn.honcharenko.dao;

import com.kostiantyn.honcharenko.entity.Product;
import com.kostiantyn.honcharenko.entity.User;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ProductDao extends CrudRepository <Product, Integer> {

    Product findByName(String login);
    Product findByProductid(int id);
    List<Product> findAll();
}
