package com.kostiantyn.honcharenko.dao;

import com.kostiantyn.honcharenko.entity.User;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface UserDao extends CrudRepository <User, Integer> {

    User findByLogin(String login);
    User findByUserid(int id);
    List<User> findAll();
}
