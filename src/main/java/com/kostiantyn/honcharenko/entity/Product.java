package com.kostiantyn.honcharenko.entity;

import javax.persistence.*;

@Entity
@Table(name = "Product")
public class Product {

    private String name;
    private int quantity;
    private double price;

    public Product () {
    }

    public Product (Integer productid, int quantity) {
        this.productid = productid;
        this.quantity = quantity;
    }

    public Product (String name, int quantity, double price) {
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer productid;

    public Integer getProductid() {
        return productid;
    }

    public void setProductid(Integer userid) {
        this.productid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void increaseQuantity (int sum) {
        this.quantity =+ sum;
    }
}
