package com.kostiantyn.honcharenko.entity;

import com.kostiantyn.honcharenko.dao.ProductDao;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Entity
@Table(name = "Users")
public class User {


    private String name;
    private String surname;
    private String login;
    private String password;
    private HashMap<Integer, Integer> boughtProducts = new HashMap<>();
    private boolean admin = false;

    public User () {
    }

    public User (String name, String surname, String login, String password, boolean admin) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.password = password;
        this.admin = admin;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer userid;

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public HashMap<Integer, Integer> getBoughtProducts() {
        return boughtProducts;
    }

    public void setBoughtProducts(HashMap<Integer, Integer> boughtProducts) {
        this.boughtProducts = boughtProducts;
    }

    public void buyProduct (int productId) {
        if (!boughtProducts.containsKey(productId)) {
            boughtProducts.put(productId, 1);
        } else {
            boughtProducts.replace(productId, boughtProducts.get(productId)+1 );
        }
    }

    /*public List<Product> getBoughtProducts() {
        return boughtProducts;
    }


    public void setBoughtProducts(List<Product> boughtProducts) {
        this.boughtProducts = boughtProducts;
    }

    public void buyProduct (Integer productId) {
       for (Product prod: boughtProducts) {
           if (prod.getProductid() == productId) {
               //Product product = new Product(productId, 1);
               prod.setQuantity(prod.getQuantity() + 1);
           } else {
               Product product = new Product(productId, 1);
               boughtProducts.add(product);
           }
       }

    }*/
}
